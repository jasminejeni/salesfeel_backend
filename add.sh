cd /var/www/html/staging
composer install
cd ..
rm staging/common/config/db.php
cp default_files/db.php staging/common/config/
cp default_files/exec.php staging/api/web/
cp default_files/company.json staging/api/web/
rsync -a staging/ legacy