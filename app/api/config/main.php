<?php

use yii\web\Response;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 've-api',
    'modules' => [
        'oauth2' => [
            'class' => 'app\modules\oauth2\Module',
        ],
        'resource' => [
            'class' => 'app\modules\resource\Module',
        ],
		'customer' => [
            'class' => 'app\modules\customer\Module',
        ],
		'sales' => [
            'class' => 'app\modules\sales\Module',
        ],
		'employee' => [
            'class' => 'app\modules\employee\Module',
        ],
        'product' => [
            'class' => 'app\modules\product\Module',
        ],
        

    ],
    'basePath' => dirname(__DIR__),
    'bootstrap' =>[
            'log',
            'contentNegotiator'=>[
                    'class' => 'yii\filters\ContentNegotiator',
                    'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    // 'application/xml' => Response::FORMAT_XML,
                    //'text/html'=>Response::FORMAT_HTML,
                    ],
                    'languages' => [
                        'en-gb',
                        'en',
                        //'de'
                    ],
            ]

    ],
    'controllerNamespace' => 'api\controllers',
    
    'components' => [

        'request' => [
            'csrfParam' => '_csrf',
            'baseUrl' => '',
            //'enableCsrfValidation' => false,
             'parsers' => [
                'application/json' => 'yii\web\JsonParser',
             ],
             'cookieValidationKey' => 'DlMDPD6o8MlZn1TZXl6mwDse28xnECIa',
        ],
         'mail' => [
         'class' => 'yii\swiftmailer\Mailer',
         'transport' => [
             'class' => 'Swift_SmtpTransport',
             'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
             'username' => 'developer@agilecyber.solutions',
             'password' => 'Acs@2018',
             'port' => '587', // Port 25 is a very common port too
             'encryption' => 'tls', // It is often used, check your provider or mail server specs
         ],
     ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession'=>'false',
            'loginUrl'=>null,
            /* enableSession has been set in RestController.php:init()  */
        //    'enableSession'=>false,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning','info','profile'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'baseUrl' => '',
            'rules' => [
                'oauth2/<controller:(.*)>/<action:(.*)>' => 'oauth2/<controller>/<action>',
                // basic set, get , delete ops on main resources
                'GET v1/<controller:(.*)>/<id:\d+>' => 'resource/v1/<controller>/index',
                'GET v1/<controller:(.*)>' => 'resource/v1/<controller>',
                'POST v1/<controller:(.*)>' => 'resource/v1/<controller>',
                'GET v1/<controller:(.*)>/search/<q:\s+>' => 'resource/v1/<controller>/<q>',
                'POST v1/<controller:(.*)>/<id:\d+>' => 'resource/v1/<controller>/create',
                'DELETE v1/<controller:(.*)>/<id:\d+>' => 'resource/v1/<controller>/delete',
                // other operations idea rating, search etc..
                'POST v1/<controller:(.*)>/<action:(.*)>' => 'resource/v1/<controller>/<action>',
                //'GET v1/<controller:(.*)>/<action:(.*)>' => 'resource/v1/<controller>/<action>',
               
			   	'GET customer/categories' => 'customer/category/list',					
				'POST customer/category/create' => 'customer/category/create',
				'POST customer/category/update' => 'customer/category/update',
				'POST customer/category/delete' => 'customer/category/delete',				
				
				'GET customer/contact/list' => 'customer/contact/list',
				'GET customer/contact/search' => 'customer/contact/search',				
				'POST customer/contact/create' => 'customer/contact/create',
				'POST customer/contact/update' => 'customer/contact/update',
				'POST customer/contact/delete' => 'customer/contact/delete',						
								
				'GET order/list' => 'sales/order/list',						
				'GET order/order_detail' => 'sales/order/view',
				'POST order/create' => 'sales/order/create',
				'POST order/update' => 'sales/order/update',
				'POST order/delete' => 'sales/order/delete',
				'GET order/history' => 'sales/order/history',
				'GET /order_stats_by/year' => 'sales/order/stats',
				'GET /order_stats_by/month' => 'sales/order/stats',

				'employee/<controller:(.*)>/<action:(.*)>' => 'employee/<controller>/<action>',	
				
				'GET product/<controller:\w+>' => 'product/<controller>/index',
				'GET product/<controller:\w+>/list' => 'product/<controller>/list',
                'GET product/<controller:\w+>/productvariants' => 'product/<controller>/productvariants',
				'GET product/<controller:\w+>/productfields' => 'product/<controller>/productfields',
                'POST product/<controller:\w+>/orderproduct' => 'product/<controller>/orderproduct',
				'POST product/<controller:\w+>/create' => 'product/<controller>/create',
				'GET product/<controller:\w+>/<id>' => 'product/<controller>/view',
				'POST product/<controller:\w+>/update' => 'product/<controller>/update',
				'POST product/<controller:\w+>/delete' => 'product/<controller>/delete',
            ],

        ],
       
        
    ],
    'params' => $params,
];
