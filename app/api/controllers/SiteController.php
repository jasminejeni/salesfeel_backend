<?php

namespace frontend\controllers;

use Yii;

/**
 * Site controller
 */
class SiteController extends RestController
{
    

    public function init()
    {
        return parent::init();
    }
    


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return true;
    }


    /**
     * Inititialise the database. 
     *
     * @return mixed
     */
    
    
}
