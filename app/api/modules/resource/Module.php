<?php

namespace app\modules\resource;

/**
 * resource module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\resource\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'v1' => [
                'class' => 'app\modules\resource\modules\v1\Module',
            ],
        ];
        // custom initialization code goes here
    }
}
