<?php

namespace app\modules\resource\controllers;

use common\components\RestController;

/**
 * Default controller for the `resource` module
 */
class DefaultController extends RestController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->throwHttpException('invalid_request');
    }
}
