<?php

namespace  app\modules\resource\modules\v1;


/**
 * v1 module definition class
 */
class Module extends \common\components\XModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\resource\modules\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();


        // custom initialization code goes here
    }
}
