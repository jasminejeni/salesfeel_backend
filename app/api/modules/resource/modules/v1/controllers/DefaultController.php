<?php

namespace app\modules\resource\modules\v1\controllers;

use common\components\RestController;

use yii;
/**
 * Default controller for the `v1` module
 */
class DefaultController extends RestController
{
    /**
     * Renders the index view for the module
     *
     * @return string
     */

    public function actionIndex()
    {

      return Yii::$app->user->identity->username;
      

    }
    
     
	
	 
}
