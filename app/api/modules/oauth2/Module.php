<?php
namespace app\modules\oauth2;

/**
 * oauth2 module definition class
 */
class Module extends \common\components\XModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\oauth2\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {

        parent::init();

        // custom initialization code goes here
    }
}
