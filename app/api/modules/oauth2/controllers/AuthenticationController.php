<?php

namespace app\modules\oauth2\controllers;

use common\components\HttpError;
use common\components\RestController;
use common\components\Types;
use common\models\User;
use Yii;
/**
 * Default controller for the `oauth2` module
 */
class AuthenticationController extends RestController
{
    public function actionToken()
    {
        $username = yii::$app->request->post('username');
        $password = yii::$app->request->post('password');
        $transientKey = yii::$app->request->post('transientKey');
        

        $storage = new \common\components\OAuthStorage();
        $validateUser = $storage->userCredentialsValidate($username,$password);
        
            
        if(empty($validateUser))
        {
            
            $validateTransientKey = $storage->validateTransientKey($transientKey);

            if($validateTransientKey)
            {
                $storage = new \common\components\OAuthStorage();
                $server = new \OAuth2\Server($storage);
                $server->addGrantType(new \OAuth2\GrantType\ClientCredentials(
                        $storage, ['allow_credentials_in_request_body' => false]
                    )
                );
                $server->addGrantType(new \OAuth2\GrantType\UserCredentials($storage));
                $server->addGrantType(new \OAuth2\GrantType\RefreshToken($storage));

                $request = \OAuth2\Request::createFromGlobals();
                $response = new \OAuth2\Response();
                $server->handleTokenRequest($request, $response);
                    if ($response->isSuccessful() ) 
                    {
                        if (isset($request->request['username']))
                        {
                            $username = $request->request['username'];
                        } 
                        else 
                        {
                            if ($request->request['grant_type'] == 'refresh_token')
                            {
                                $username = $storage->getUserIdFromRefreshToken('oauth_refresh_token',
                                            ['refresh_token'=>$request->request['refresh_token']]);
                            }
                        }

                        $clientId = $request->headers['PHP_AUTH_USER'];
                        $params = $response->getParameters();
                        
                        $storage->setAccessToken(
                            $params['access_token'],
                            $clientId,
                            $username,
                            time() + $params['expires_in'],
                            $params['scope']
                        );

                        if ($request->request['grant_type'] != 'refresh_token')
                        {
                            $storage->setRefreshToken(
                                $params['refresh_token'],
                                $clientId,
                                $username,
                                time() + $params['expires_in'] + ((60 * 24) * 28),
                                $params['scope']
                            );
                        }
                        return $params;
                    }
                    else
                    {
                        return $response->getParameters();
                    }
           }
           else
           {
                $data['error'] = Yii::t('app', Yii::$app->params['invalidTransientKey']);
                return $data;
           }
        }
        else
        {
            $data['error'] = $validateUser;
            return $data;
        }
        
    }

    public function actionRegister()
    {
        $username = yii::$app->request->post('username');
        $password = yii::$app->request->post('password');
        
        $user  =  User::findOne(['username'=>$username]);
        
        if ($user !== null)
            return $this->throwHttpException('invalid_request');


        $user = new User;
        $user->username = $username;
        $user->password = $password;

        if (! $user->save())
        {
            return $this->throwHttpException('invalid_request');
        }
        
        
        $storage = new \common\components\OAuthStorage();
        $server = new \OAuth2\Server($storage);
        $server->addGrantType(new \OAuth2\GrantType\ClientCredentials(
                $storage, ['allow_credentials_in_request_body' => false]
            )
        );
        $server->addGrantType(new \OAuth2\GrantType\UserCredentials($storage));
        $server->addGrantType(new \OAuth2\GrantType\RefreshToken($storage));

        $request = \OAuth2\Request::createFromGlobals();

        $response = new \OAuth2\Response();

        $server->handleTokenRequest($request, $response);
        if ($response->isSuccessful() ) {
            if (isset($request->request['username']))
            {
                $username = $request->request['username'];
            } 
            else 
            {
                if ($request->request['grant_type'] == 'refresh_token')
                {
                    $username = $storage->getUserIdFromRefreshToken('oauth_refresh_token',
                        ['refresh_token'=>$request->request['refresh_token']]);
                }
            }
            $client_id = $request->headers['PHP_AUTH_USER'];
            $params = $response->getParameters();


            $storage->setAccessToken(
                $params['access_token'],
                $client_id,
                $username,
                time() + $params['expires_in'],
                $params['scope']
            );

            if ($request->request['grant_type'] != 'refresh_token')
            {
                $storage->setRefreshToken(
                    $params['refresh_token'],
                    $client_id,
                    $username,
                    time() + $params['expires_in'] + ((60 * 24) * 28),
                    $params['scope']
                );
            }
            return $params;
        }
        else
        {
            return $response->getParameters();
        }
    }

}
