CREATE TABLE IF NOT EXISTS `super_distributor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor` varchar(255) NOT NULL,
  `record_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(6) NOT NULL,
  `updated_by` int(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
