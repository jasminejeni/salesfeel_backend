--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sub_category_id`, `record_status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'Product 1', 1, 1, '2019-06-04 09:10:52', '2019-06-04 09:10:52', 5, 5, 0),
(2, 'Product 2', 2, 1, '2019-06-04 09:11:54', '2019-06-04 09:11:54', 2, 2, 0);