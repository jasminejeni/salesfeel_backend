SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE `product_categories`;
TRUNCATE `products`;
TRUNCATE `product_attribute_values`;

SET FOREIGN_KEY_CHECKS = 1;