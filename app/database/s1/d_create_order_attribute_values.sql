
--
-- Table structure for table `order_attribute_values`
--

CREATE TABLE `order_attribute_values` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `record_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_attribute_values`
--
ALTER TABLE `order_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_attribute_values_fk0` (`attribute_id`),
  ADD KEY `order_attribute_values_fk1` (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_attribute_values`
--
ALTER TABLE `order_attribute_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_attribute_values`
--
ALTER TABLE `order_attribute_values`
  ADD CONSTRAINT `order_attribute_values_fk0` FOREIGN KEY (`attribute_id`) REFERENCES `order_attributes` (`id`),
  ADD CONSTRAINT `order_attribute_values_fk1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
