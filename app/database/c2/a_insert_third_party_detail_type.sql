
TRUNCATE TABLE `third_party_detail_type`;

INSERT INTO `third_party_detail_type` (`id`, `name`, `record_status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'super_distributor_id', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(2, 'distributor_id', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(3, 'shipping_address', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(4, 'billing_address', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(5, 'mobile_no', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(6, 'email', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(7, 'gstin', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(8, 'pan', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(9, 'aadhar_no', 1, '2019-05-29 05:20:26', '2019-05-29 05:20:26', NULL, NULL, 0),
(10, 'uid', 1, '2019-05-29 07:19:19', '2019-06-06 00:10:59', NULL, NULL, 0),
(11, 'last_visited', 1, '2019-06-06 00:11:33', '2019-06-06 00:11:33', NULL, NULL, 0);