
--
-- Table structure for table `third_party_details`
--

CREATE TABLE `third_party_details` (
  `id` int(11) NOT NULL,
  `third_party_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `detail_type` int(11) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `priority_order` int(11) DEFAULT NULL,
  `record_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table `third_party_details`
--
ALTER TABLE `third_party_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `third_party_details_fk0` (`third_party_id`),
  ADD KEY `third_party_details_fk1` (`contact_id`),
  ADD KEY `third_party_details_fk2` (`location_id`),
  ADD KEY `third_party_details_fk3` (`detail_type`);
  

--
-- AUTO_INCREMENT for table `third_party_details`
--
ALTER TABLE `third_party_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
  
  
--
-- Constraints for table `third_party_details`
--
ALTER TABLE `third_party_details`
  ADD CONSTRAINT `third_party_details_fk0` FOREIGN KEY (`third_party_id`) REFERENCES `third_parties` (`id`),
  ADD CONSTRAINT `third_party_details_fk1` FOREIGN KEY (`contact_id`) REFERENCES `third_party_contacts` (`id`),
  ADD CONSTRAINT `third_party_details_fk2` FOREIGN KEY (`location_id`) REFERENCES `third_party_locations` (`id`),
  ADD CONSTRAINT `third_party_details_fk3` FOREIGN KEY (`detail_type`) REFERENCES `third_party_detail_type` (`id`);