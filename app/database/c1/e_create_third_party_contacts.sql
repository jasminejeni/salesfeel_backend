
--
-- Table structure for table `third_party_contacts`
--

CREATE TABLE `third_party_contacts` (
  `id` int(11) NOT NULL,
  `third_party_id` int(11) NOT NULL,
  `third_party_location_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `date_of_anniversary` date DEFAULT NULL,
  `record_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `gender` enum('Male','Female','Other') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table `third_party_contacts`
--
ALTER TABLE `third_party_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `third_party_contacts_fk0` (`third_party_id`),
  ADD KEY `third_party_contacts_fk1` (`third_party_location_id`);
  
--
-- AUTO_INCREMENT for table `third_party_contacts`
--
ALTER TABLE `third_party_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
 --
-- Constraints for table `third_party_contacts`
--
ALTER TABLE `third_party_contacts`
  ADD CONSTRAINT `third_party_contacts_fk0` FOREIGN KEY (`third_party_id`) REFERENCES `third_parties` (`id`),
  ADD CONSTRAINT `third_party_contacts_fk1` FOREIGN KEY (`third_party_location_id`) REFERENCES `third_party_locations` (`id`);