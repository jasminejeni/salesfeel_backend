--
-- Dumping data for table `location_types`
--

INSERT INTO `location_types` (`id`, `name`, `record_status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'Headquarters', 1, '2019-05-29 06:09:13', '2019-05-29 06:09:13', 1, 1, 0),
(2, 'Shipping', 1, '2019-05-29 06:08:01', '2019-05-29 06:08:01', 1, 1, 0),
(3, 'Billing', 1, '2019-05-29 06:08:01', '2019-05-29 06:08:01', 1, 1, 0),
(4, 'Shop', 1, '2019-05-29 06:08:25', '2019-05-29 06:08:25', 1, 1, 0),
(5, 'Factory', 1, '2019-05-29 06:08:25', '2019-05-29 06:08:25', 1, 1, 0);