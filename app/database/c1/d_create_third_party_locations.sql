
--
-- Table structure for table `third_party_locations`
--

CREATE TABLE `third_party_locations` (
  `id` int(11) NOT NULL,
  `third_party_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `priority_order` int(11) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `record_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table `third_party_locations`
--
ALTER TABLE `third_party_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `third_party_locations_fk1` (`type`),
  ADD KEY `third_party_locations_fk0` (`third_party_id`) USING BTREE;


--
-- AUTO_INCREMENT for table `third_party_locations`
--
ALTER TABLE `third_party_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
  
  --
-- Constraints for table `third_party_locations`
--
ALTER TABLE `third_party_locations`
  ADD CONSTRAINT `third_party_locations_fk0` FOREIGN KEY (`third_party_id`) REFERENCES `third_parties` (`id`),
  ADD CONSTRAINT `third_party_locations_fk1` FOREIGN KEY (`type`) REFERENCES `location_types` (`id`);
