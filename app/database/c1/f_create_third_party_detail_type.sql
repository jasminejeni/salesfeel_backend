
--
-- Table structure for table `third_party_detail_type`
--

CREATE TABLE `third_party_detail_type` (
  `id` int(11) NOT NULL,  
  `name` varchar(255) DEFAULT NULL,
  `record_status` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table `third_party_detail_type`
--
ALTER TABLE `third_party_detail_type`
  ADD PRIMARY KEY (`id`);


--
-- AUTO_INCREMENT for table `third_party_detail_type`
--
ALTER TABLE `third_party_detail_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
