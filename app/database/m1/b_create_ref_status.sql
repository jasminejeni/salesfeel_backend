CREATE TABLE `ref_status` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(512) DEFAULT NULL,
  `name` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `display` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '100',
  `bit_flag` int(11) DEFAULT '0',
  `modified_flag` int(10) DEFAULT '0',
  `status_id` int(10) UNSIGNED DEFAULT '1',
  `create_time` int(10) UNSIGNED NOT NULL,
  `create_usr_id` int(10) UNSIGNED NOT NULL,
  `update_time` int(10) UNSIGNED DEFAULT NULL,
  `update_usr_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

