
CREATE TABLE `log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `action_id` int(10) NOT NULL DEFAULT '1',
  `model_id` int(10) NOT NULL DEFAULT '1',
  `user_agent` varchar(255) DEFAULT NULL,
  `remote_addr` varchar(64) DEFAULT NULL,
  `attribute_dump` varchar(4096) DEFAULT NULL,
  `record_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(6) DEFAULT NULL,
  `updated_by` int(6) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_log_record_status` (`record_status`),
  KEY `fk_log_created_at` (`created_at`),
  CONSTRAINT `fk_log_record_status` FOREIGN KEY (`record_status`) REFERENCES `ref_status` (`id`),
  CONSTRAINT `fk_log_created_at` FOREIGN KEY (`created_at`) REFERENCES `usr` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

