INSERT INTO `ref_status` (`id`, `code`, `name`, `description`, `title`, `display`, `sort_order`, `bit_flag`, `modified_flag`, `status_id`, `create_time`, `create_usr_id`, `update_time`, `update_usr_id`) VALUES
(1, 'NULL', 'Select from...', 'No value', NULL, 0, 100, 0, 0, 1, 1336465334, 1, NULL, NULL),
(2, 'active', 'Active', 'Go live', NULL, 1, 100, 0, 0, 2, 1545376750, 1, NULL, NULL),
(3, 'inactive', 'Inactive', 'Make inactive', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(4, 'deleted', 'Deleted', 'Deleted', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(5, 'disabled', 'Disabledq', 'Disabled', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(6, 'archived', 'Archived', 'Archived', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(7, 'cancelled', 'Cancelled', 'Cancelled', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(8, 'pending', 'Pending', 'Under review', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(9, 'draft', 'Draft', 'Draft', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL),
(10, 'restricted', 'Restricted', 'Restricted', NULL, 1, 100, 0, 0, 2, 1336465334, 1, NULL, NULL);