--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `name`, `record_status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'code', 1, '2019-05-30 08:17:12', '2019-05-30 08:17:12', NULL, NULL, 0),
(2, 'hsn', 1, '2019-05-30 08:17:12', '2019-05-30 08:17:12', NULL, NULL, 0),
(3, 'image_url', 1, '2019-05-30 08:18:12', '2019-05-30 08:18:12', NULL, NULL, 0),
(4, 'keywords', 1, '2019-05-30 08:18:37', '2019-05-30 08:18:37', NULL, NULL, 0),
(5, 'margin', 1, '2019-05-30 08:18:52', '2019-05-30 08:18:52', NULL, NULL, 0),
(6, 'margin_character', 1, '2019-05-30 08:19:10', '2019-05-30 08:19:10', NULL, NULL, 0),
(7, 'order', 1, '2019-05-30 08:19:32', '2019-05-30 08:19:32', NULL, NULL, 0),
(8, 'popular', 1, '2019-05-30 08:19:50', '2019-05-30 08:19:50', NULL, NULL, 0),
(9, 'price', 1, '2019-05-30 08:20:04', '2019-05-30 08:20:04', NULL, NULL, 0),
(10, 'unit', 1, '2019-05-30 08:20:17', '2019-05-30 08:20:17', NULL, NULL, 0),
(11, 'variants', 1, '2019-05-30 08:20:31', '2019-05-30 08:20:31', NULL, NULL, 0),
(12, 'volume_character', 1, '2019-05-30 08:20:52', '2019-05-30 08:20:52', NULL, NULL, 0);