<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'settingUrl' => 'https://salesfeel.org/',
    'invalidTransientKey' => 'Invalid Transient Key',
    'invalidToken' => 'Invalid token',
    'invalidUsernamePassword' => 'Invalid username and password combination',
    'invalidAccessCode' => 'Access code is cannot be found or is no longer valid',
    'sysAdmin' => 'sysAdmin',
    'invalidPassword' => 'Invalid Password',
    'emailUsernameNotRecognised' => 'Email / Username not recognised',
];
