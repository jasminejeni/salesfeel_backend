<?php
$db = require __DIR__ . '/db.php';

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'arangoDB' => [
            'class'=>'common\components\DbComponent', 
        ],
        'OAuthComponent' => [
            'class'=>'common\components\OAuthComponent', 
        ],
        'UserComponent' => [
            'class'=>'common\components\UserComponent',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'php:c',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'GBP',
            'defaultTimeZone' => 'UTC', // time zone
        ],
		'api' => [
            'class'=>'common\components\Api', 
        ],
		'db' => $db
    ],
];
