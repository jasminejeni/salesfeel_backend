<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "oauth_access_tokens".
 *
 * @property string $access_token
 * @property string $client_id
 * @property int $user_id
 * @property string $expires
 * @property string $scope
 *
 * @property OauthClients $client
 */
class OAuthAccessToken extends \common\components\XActiveRecord
{

    public $created_by;
    public $updated_by;
    public $created_at;
    public $updated_at;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_access_tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_token', 'client_id'], 'required'],
            [['user_id'], 'integer'],
            [['expires','created_by','updated_by'], 'safe'],
            [['access_token'], 'string', 'max' => 40],
            [['client_id'], 'string', 'max' => 80],
            [['scope'], 'string', 'max' => 2000],
            [['access_token'], 'unique'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => OAuthClient::className(), 'targetAttribute' => ['client_id' => 'client_id']],
            ];
    }




    public function afterFind()
    {
        // convert expires to datestring
        $this->expires = \Yii::$app->formatter->asDatetime($this->expires);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'access_token' => Yii::t('app', 'Access Token'),
            'client_id' => Yii::t('app', 'Client ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'expires' => Yii::t('app', 'Expires'),
            'scope' => Yii::t('app', 'Scope'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(OauthClients::className(), ['client_id' => 'client_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
