<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "oauth_refresh_tokens".
 *
 * @property string $refresh_token
 * @property string $client_id
 * @property int $user_id
 * @property string $expires
 * @property string $scope
 *
 * @property OauthClients $client
 */
class OAuthRefreshToken extends \common\components\XActiveRecord
{

    public $created_by;
    public $updated_by;
    public $created_at;
    public $updated_at;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_refresh_tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['refresh_token', 'client_id'], 'required'],
            [['user_id'], 'integer'],
            [['expires','created_by','updated_by'], 'safe'],
            [['refresh_token'], 'string', 'max' => 40],
            [['client_id'], 'string', 'max' => 80],
            [['scope'], 'string', 'max' => 2000],
            [['refresh_token'], 'unique'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => OAuthClient::className(), 'targetAttribute' => ['client_id' => 'client_id']],
        ];
    }


    public function fields()
    {
        return [
            'refresh_token', 'client_id' ,
            'user_id',  'expires' , 'scope'
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'refresh_token' => Yii::t('app', 'Refresh Token'),
            'client_id' => Yii::t('app', 'Client ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'expires' => Yii::t('app', 'Expires'),
            'scope' => Yii::t('app', 'Scope'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(OauthClients::className(), ['client_id' => 'client_id']);
    }
}
