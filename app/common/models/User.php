<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use common\components\Types;


class User extends \common\components\XActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usr';
    }

    public $user_id;
    /**
     * @inheritdoc
     */


    public function beforeValidate()
    {
        $this->created_by = 1;
        return parent::beforeValidate();
    }
    public function beforeSave($insert)
    {

        if ($insert)
        {
            $this->hash_type = 'bcrypt';
            $this->password =  Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        return parent::beforeSave($insert);
    }
    public function afterFind()
    {
        parent::afterFind();
        // to satisfy a requirement of OAuthStorage
        $this->user_id = $this->id ;
        $log = new Log;
        $log->model_id = Types::$modelType['null']['id'];
        $log->action_id = Types::$actionType['login']['id'];
        $log->save();
    }

    public function fields(){

        return array_merge (array_keys($this->attributes), ['user_id']);
    }
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['created_by','updated_by'], 'integer'],
            [['username', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['record_status','deleted'], 'boolean'],
            [['user_id','created_at','updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'record_status' => Yii::t('app', 'Status ID'),
            'created_at' => Yii::t('app', 'Create Time'),
            'created_by' => Yii::t('app', 'Create User ID'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'updated_by' => Yii::t('app', 'Update User ID'),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {

    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {



        $data =  OAuthAccessToken::find()->where(['=', 'access_token' ,  $token])
                                            ->andWhere(['>' , 'expires', time()])
                                            ->all();
        if ($data) {
            return $data[0]->user;

        }
        else
            return null;

    }

    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {

    }

    /**
     * Finds user by password reset token
     *
     * @param  string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {

    }

    /**
     * Finds out if password reset token is valid
     *
     * @param  string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {

    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {

    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {

    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {

    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {

    }
}
