<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "super_distributor".
 *
 * @property int $id
 * @property string $distributor
 * @property int $record_status
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $deleted
 */
class SuperDistributor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'super_distributor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['distributor', 'created_by', 'updated_by'], 'required'],
            [['record_status', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['distributor'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'distributor' => 'Distributor',
            'record_status' => 'Record Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }
}
