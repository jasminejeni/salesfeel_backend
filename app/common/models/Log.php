<?php
namespace common\models;
use common\components\Types;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "log".
 *
 * The followings are the available columns in table 'log':
 * @property string $id
 * @property string $action_id
 * @property string $model_id
 * @property string $obj_id
 * @property string $grp_id
 * @property string $model_status_id
 * @property string $user_agent
 * @property string $remote_addr
 * @property string $note
 * @property string $attribute_dump
 * @property string $modified_flag
 * @property string $status_id
 * @property string $create_time
 * @property string $create_usr_id
 * @property string $update_time
 * @property string $update_usr_id
 *
 * The followings are the available model relations:
 * @property RefStatus $status
 * @property Usr $createUsr
 * @property RefAction $action
 * @property RefModel $model
 * @property Grp $grp
 */
class Log extends \common\components\XActiveRecord implements IdentityInterface
{

	
	
	public static function tableName()
    {
		return 'log';
	}
	
	
	
	
	
	/* *********************************************** */
	public function beforeValidate()
	{
		$this->created_by = 1;
		if (is_array($this->attribute_dump))
			$this->attribute_dump = serialize($this->attribute_dump); 
		return parent::beforeValidate(); 
	} 
	/* *********************************************** */
	
	public function beforeSave($insert)
	{
		
		$this->user_agent = Yii::$app->request->userAgent;
		$this->remote_addr = Yii::$app->getRequest()->getUserIP();
		$this->record_status = Types::$status['active']['id']; 
		return parent::beforeSave($insert); 
	} 
	
	/* *********************************************** */

	
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			[['action_id', 'model_id', 'record_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
			[['user_agent'], 'string', 'max'=>255],
			[['remote_addr'], 'string', 'max'=>64],
			[['attribute_dump'], 'string', 'max'=>4096],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			[['id', 'action_id', 'model_id',  'attribute_dump', 'user_agent', 'remote_addr',  'record_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe', 'on'=>'search'],
		];
	}
	/* *********************************************** */

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status' => array(self::BELONGS_TO, 'RefStatus', 'record_status'),
			'createUsr' => array(self::BELONGS_TO, 'Usr', 'created_by'),
			'action' => array(self::BELONGS_TO, 'RefAction', 'action_id'),
			'model' => array(self::BELONGS_TO, 'RefModel', 'model_id'),
			'grp' => array(self::BELONGS_TO, 'Grp', 'grp_id'),
		);
	}
	/* *********************************************** */

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app','ID'),
			'action_id' => Yii::t('app','Action'),
			'model_id' => Yii::t('app','Model'),
			'user_agent' => Yii::t('app','User Agent'),
			'remote_addr' => Yii::t('app','Remote Addr'),
			'attribute_dump' => Yii::t('app','Attributes'),
			'record_status' => Yii::t('app','Status'),
			'created_at' => Yii::t('app','Create Time'),
			'created_by' => Yii::t('app','Create Usr'),
			'updated_at' => Yii::t('app','Update Time'),
			'updated_by' => Yii::t('app','Update Usr'),
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('action_id',$this->action_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('remote_addr',$this->remote_addr,true);
		$criteria->compare('attribute_dump',$this->attribute_dump,true);
		$criteria->compare('record_status',$this->record_status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_at',$this->update_time,true);
		$criteria->compare('updated_by',$this->update_usr_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function findIdentity($id)
    {

    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {



        $data =  OAuthAccessToken::find()->where(['=', 'access_token' ,  $token])
                                            ->andWhere(['>' , 'expires', time()])
                                            ->all();
        if ($data) {
            return $data[0]->user;

        }
        else
            return null;

	}
	
	public static function findByUsername($username)
    {

    }

    /**
     * Finds user by password reset token
     *
     * @param  string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {

    }

    /**
     * Finds out if password reset token is valid
     *
     * @param  string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {

    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {

    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {

    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {

    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {

    }
}