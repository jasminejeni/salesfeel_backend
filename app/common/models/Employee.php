<?php

namespace common\models;

use Yii;

class Employee extends \common\components\XActiveRecord
{
    public static function tableName()
    {
        return 'employees';
    }

    public function beforeValidate()
    {
        $this->created_by = 1;
        return parent::beforeValidate();
    }

    public function rules()
    {
        return [
            [['user_id', 'employee_firstname','mobile_no','email','user_role'], 'required'],
            [['created_by','updated_by','user_id','manager_id','state','super_distributor','user_role'], 'integer'],
            [['email', 'address','designation'], 'string', 'max' => 255],
            [['employee_firstname', 'employee_lastname','emp_code'], 'string', 'max' => 60],
            [['headquarters', 'zone', 'area'], 'string', 'max' => 150],
            [['mobile_no'], 'string', 'max' => 12],
            [['joint_working'], 'string', 'max' => 6],
            [['gender'], 'string', 'max' => 10],
            [['record_status','deleted'], 'boolean'],
            [['created_at','updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'UserId'),
            'employee_firstname' => Yii::t('app', 'Firstname'),
            'employee_lastname' => Yii::t('app', 'Lastname'),
            'email' => Yii::t('app', 'Email'),
            'address' => Yii::t('app', 'Address'),
            'mobile_no' => Yii::t('app', 'Phone'),
            'hired_date' => Yii::t('app', 'Hired date'),
            'designation' => Yii::t('app', 'Designation'),
            'dob' => Yii::t('app', 'DOB'),
            'gender' => Yii::t('app', 'Gender'),
            'record_status' => Yii::t('app', 'Status ID'),
            'created_at' => Yii::t('app', 'Create Time'),
            'created_by' => Yii::t('app', 'Create User ID'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'updated_by' => Yii::t('app', 'Update User ID'),
        ];
    }
}
