<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ref_status".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $title
 * @property int $display
 * @property int $sort_order
 * @property int $bit_flag
 * @property int $modified_flag
 * @property int $status_id
 * @property int $create_time
 * @property int $create_usr_id
 * @property int $update_time
 * @property int $update_usr_id
 *
 * @property Act[] $acts
 * @property Adr[] $adrs
 * @property Blog[] $blogs
 * @property Budget[] $budgets
 * @property Cmt[] $cmts
 * @property CmtRepl[] $cmtRepls
 * @property Ctc[] $ctcs
 * @property Event[] $events
 * @property Feedback[] $feedbacks
 * @property Grp[] $grps
 * @property GrpRel[] $grpRels
 * @property GrpScoreboard[] $grpScoreboards
 * @property GrpScoreboard[] $grpScoreboards0
 * @property GrpUsrPref[] $grpUsrPrefs
 * @property History[] $histories
 * @property Idea[] $eas
 * @property IdeaRtg[] $eaRtgs
 * @property IdeaScoreboard[] $eaScoreboards
 * @property InappropriateContent[] $inappropriateContents
 * @property Jtc[] $jtcs
 * @property LnkActIdea[] $lnkActIdeas
 * @property LnkActOutcome[] $lnkActOutcomes
 * @property LnkBudgetTpc[] $lnkBudgetTpcs
 * @property LnkGrpCat[] $lnkGrpCats
 * @property LnkOembedModel[] $lnkOembedModels
 * @property LnkTpcCat[] $lnkTpcCats
 * @property LnkTpcGrp[] $lnkTpcGrps
 * @property LnkTpcStakeholder[] $lnkTpcStakeholders
 * @property LnkTpcTargetGrpType[] $lnkTpcTargetGrpTypes
 * @property LnkUploadModel[] $lnkUploadModels
 * @property LnkUsrCmtRepl[] $lnkUsrCmtRepls
 * @property LnkUsrGrp[] $lnkUsrGrps
 * @property LnkWgtSecurityOption[] $lnkWgtSecurityOptions
 * @property Log[] $logs
 * @property Message[] $messages
 * @property MessageReceiver[] $messageReceivers
 * @property Motion[] $motions
 * @property MotionCmt[] $motionCmts
 * @property MotionCmtRepl[] $motionCmtRepls
 * @property MotionVoting[] $motionVotings
 * @property Note[] $notes
 * @property NotificationLog[] $notificationLogs
 * @property NotificationRun[] $notificationRuns
 * @property Oembed[] $oembeds
 * @property Proj[] $projs
 * @property PwdReset[] $pwdResets
 * @property RefAction[] $refActions
 * @property RefAdrType[] $refAdrTypes
 * @property RefBoundary[] $refBoundaries
 * @property RefCat[] $refCats
 * @property RefClientType[] $refClientTypes
 * @property RefConnectionType[] $refConnectionTypes
 * @property RefContentProvider[] $refContentProviders
 * @property RefCountType[] $refCountTypes
 * @property RefCtcType[] $refCtcTypes
 * @property RefDeclineReason[] $refDeclineReasons
 * @property RefEntryType[] $refEntryTypes
 * @property RefEventType[] $refEventTypes
 * @property RefExp[] $refExps
 * @property RefFlagInappropriate[] $refFlagInappropriates
 * @property RefFont[] $refFonts
 * @property RefGender[] $refGenders
 * @property RefGrpClass[] $refGrpClasses
 * @property RefGrpType[] $refGrpTypes
 * @property RefLoc[] $refLocs
 * @property RefMemType[] $refMemTypes
 * @property RefOpinion[] $refOpinions
 * @property RefOutcome[] $refOutcomes
 * @property RefRelType[] $refRelTypes
 * @property RefSecurityOption[] $refSecurityOptions
 * @property RefService[] $refServices
 * @property RefStakeholderType[] $refStakeholderTypes
 * @property RefStatus $status
 * @property RefStatus[] $refStatuses
 * @property RefSupportPhase[] $refSupportPhases
 * @property RefSymbol[] $refSymbols
 * @property RefTitle[] $refTitles
 * @property RefTpcType[] $refTpcTypes
 * @property RefTransactionType[] $refTransactionTypes
 * @property RefTransmissionType[] $refTransmissionTypes
 * @property RefTruth[] $refTruths
 * @property RefUnit[] $refUnits
 * @property RefUploadType[] $refUploadTypes
 * @property RefVotingOption[] $refVotingOptions
 * @property RefWflow[] $refWflows
 * @property Report[] $reports
 * @property ReportPref[] $reportPrefs
 * @property Resource[] $resources
 * @property ResourcePledge[] $resourcePledges
 * @property Stash[] $stashes
 * @property SystemSetting[] $systemSettings
 * @property Template[] $templates
 * @property Tpc[] $tpcs
 * @property Transaction[] $transactions
 * @property Upload[] $uploads
 * @property Usr[] $usrs
 * @property UsrPref[] $usrPrefs
 * @property UsrPseudonym[] $usrPseudonyms
 * @property Vfy[] $vfies
 * @property Wgt[] $wgts
 */
class RefStatus extends \common\components\XActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ref_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name', 'description', 'create_time', 'create_usr_id'], 'required'],
            [['display', 'sort_order', 'bit_flag', 'modified_flag', 'status_id', 'create_time', 'create_usr_id', 'update_time', 'update_usr_id'], 'integer'],
            [['code', 'name', 'description'], 'string', 'max' => 512],
            [['title'], 'string', 'max' => 128],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'title' => Yii::t('app', 'Title'),
            'display' => Yii::t('app', 'Display'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'bit_flag' => Yii::t('app', 'Bit Flag'),
            'modified_flag' => Yii::t('app', 'Modified Flag'),
            'status_id' => Yii::t('app', 'Status ID'),
            'create_time' => Yii::t('app', 'Create Time'),
            'create_usr_id' => Yii::t('app', 'Create Usr ID'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_usr_id' => Yii::t('app', 'Update Usr ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(Act::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdrs()
    {
        return $this->hasMany(Adr::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogs()
    {
        return $this->hasMany(Blog::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBudgets()
    {
        return $this->hasMany(Budget::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmts()
    {
        return $this->hasMany(Cmt::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmtRepls()
    {
        return $this->hasMany(CmtRepl::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCtcs()
    {
        return $this->hasMany(Ctc::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrps()
    {
        return $this->hasMany(Grp::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrpRels()
    {
        return $this->hasMany(GrpRel::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrpScoreboards()
    {
        return $this->hasMany(GrpScoreboard::className(), ['count_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrpScoreboards0()
    {
        return $this->hasMany(GrpScoreboard::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrpUsrPrefs()
    {
        return $this->hasMany(GrpUsrPref::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEas()
    {
        return $this->hasMany(Idea::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEaRtgs()
    {
        return $this->hasMany(IdeaRtg::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEaScoreboards()
    {
        return $this->hasMany(IdeaScoreboard::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInappropriateContents()
    {
        return $this->hasMany(InappropriateContent::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJtcs()
    {
        return $this->hasMany(Jtc::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkActIdeas()
    {
        return $this->hasMany(LnkActIdea::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkActOutcomes()
    {
        return $this->hasMany(LnkActOutcome::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkBudgetTpcs()
    {
        return $this->hasMany(LnkBudgetTpc::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkGrpCats()
    {
        return $this->hasMany(LnkGrpCat::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkOembedModels()
    {
        return $this->hasMany(LnkOembedModel::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkTpcCats()
    {
        return $this->hasMany(LnkTpcCat::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkTpcGrps()
    {
        return $this->hasMany(LnkTpcGrp::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkTpcStakeholders()
    {
        return $this->hasMany(LnkTpcStakeholder::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkTpcTargetGrpTypes()
    {
        return $this->hasMany(LnkTpcTargetGrpType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkUploadModels()
    {
        return $this->hasMany(LnkUploadModel::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkUsrCmtRepls()
    {
        return $this->hasMany(LnkUsrCmtRepl::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkUsrGrps()
    {
        return $this->hasMany(LnkUsrGrp::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkWgtSecurityOptions()
    {
        return $this->hasMany(LnkWgtSecurityOption::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageReceivers()
    {
        return $this->hasMany(MessageReceiver::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotions()
    {
        return $this->hasMany(Motion::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotionCmts()
    {
        return $this->hasMany(MotionCmt::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotionCmtRepls()
    {
        return $this->hasMany(MotionCmtRepl::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotionVotings()
    {
        return $this->hasMany(MotionVoting::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(Note::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationLogs()
    {
        return $this->hasMany(NotificationLog::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationRuns()
    {
        return $this->hasMany(NotificationRun::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOembeds()
    {
        return $this->hasMany(Oembed::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjs()
    {
        return $this->hasMany(Proj::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPwdResets()
    {
        return $this->hasMany(PwdReset::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefActions()
    {
        return $this->hasMany(RefAction::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefAdrTypes()
    {
        return $this->hasMany(RefAdrType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefBoundaries()
    {
        return $this->hasMany(RefBoundary::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefCats()
    {
        return $this->hasMany(RefCat::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefClientTypes()
    {
        return $this->hasMany(RefClientType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefConnectionTypes()
    {
        return $this->hasMany(RefConnectionType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefContentProviders()
    {
        return $this->hasMany(RefContentProvider::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefCountTypes()
    {
        return $this->hasMany(RefCountType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefCtcTypes()
    {
        return $this->hasMany(RefCtcType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefDeclineReasons()
    {
        return $this->hasMany(RefDeclineReason::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefEntryTypes()
    {
        return $this->hasMany(RefEntryType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefEventTypes()
    {
        return $this->hasMany(RefEventType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefExps()
    {
        return $this->hasMany(RefExp::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefFlagInappropriates()
    {
        return $this->hasMany(RefFlagInappropriate::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefFonts()
    {
        return $this->hasMany(RefFont::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefGenders()
    {
        return $this->hasMany(RefGender::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefGrpClasses()
    {
        return $this->hasMany(RefGrpClass::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefGrpTypes()
    {
        return $this->hasMany(RefGrpType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefLocs()
    {
        return $this->hasMany(RefLoc::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefMemTypes()
    {
        return $this->hasMany(RefMemType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefOpinions()
    {
        return $this->hasMany(RefOpinion::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefOutcomes()
    {
        return $this->hasMany(RefOutcome::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefRelTypes()
    {
        return $this->hasMany(RefRelType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefSecurityOptions()
    {
        return $this->hasMany(RefSecurityOption::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefServices()
    {
        return $this->hasMany(RefService::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefStakeholderTypes()
    {
        return $this->hasMany(RefStakeholderType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(RefStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefStatuses()
    {
        return $this->hasMany(RefStatus::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefSupportPhases()
    {
        return $this->hasMany(RefSupportPhase::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefSymbols()
    {
        return $this->hasMany(RefSymbol::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefTitles()
    {
        return $this->hasMany(RefTitle::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefTpcTypes()
    {
        return $this->hasMany(RefTpcType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefTransactionTypes()
    {
        return $this->hasMany(RefTransactionType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefTransmissionTypes()
    {
        return $this->hasMany(RefTransmissionType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefTruths()
    {
        return $this->hasMany(RefTruth::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefUnits()
    {
        return $this->hasMany(RefUnit::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefUploadTypes()
    {
        return $this->hasMany(RefUploadType::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefVotingOptions()
    {
        return $this->hasMany(RefVotingOption::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefWflows()
    {
        return $this->hasMany(RefWflow::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportPrefs()
    {
        return $this->hasMany(ReportPref::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResourcePledges()
    {
        return $this->hasMany(ResourcePledge::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStashes()
    {
        return $this->hasMany(Stash::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSystemSettings()
    {
        return $this->hasMany(SystemSetting::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(Template::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTpcs()
    {
        return $this->hasMany(Tpc::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploads()
    {
        return $this->hasMany(Upload::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsrs()
    {
        return $this->hasMany(Usr::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsrPrefs()
    {
        return $this->hasMany(UsrPref::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsrPseudonyms()
    {
        return $this->hasMany(UsrPseudonym::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVfies()
    {
        return $this->hasMany(Vfy::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWgts()
    {
        return $this->hasMany(Wgt::className(), ['status_id' => 'id']);
    }
}
