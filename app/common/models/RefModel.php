<?php
namespace common\models;
use Yii;
/**
 * This is the model class for table "ref_model".
 *
 * The followings are the available columns in table 'ref_model':
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $title
 * @property integer $display
 * @property integer $sort_order
 * @property integer $bit_flag
 * @property string $modified_flag
 * @property string $status_id
 * @property string $create_time
 * @property string $create_usr_id
 * @property string $update_time
 * @property string $update_usr_id
 *
 * The followings are the available model relations:
 * @property GrpScoreboard[] $grpScoreboards
 * @property Log[] $logs
 * @property RefModel $status
 * @property RefModel[] $refModels
 */
class RefModel extends \common\components\XActiveRecord
{
	

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ref_model';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			[['code, name, description, create_time, create_usr_id'], 'required'],
			[['display, sort_order, bit_flag'], 'integer'],
			[['code, name, description'], 'string', 'max'=>512],
			[['title'], 'string', 'max'=>128],
			[['modified_flag, status_id, create_time, create_usr_id, update_time, update_usr_id'], 'integer'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			[['id, code, name, description, title, display, sort_order, bit_flag, modified_flag, status_id, create_time, create_usr_id, update_time, update_usr_id'], 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'grpScoreboards' => array(self::HAS_MANY, 'GrpScoreboard', 'model_id'),
			'logs' => array(self::HAS_MANY, 'Log', 'model_id'),
			'status' => array(self::BELONGS_TO, 'RefModel', 'status_id'),
			'refModels' => array(self::HAS_MANY, 'RefModel', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'code' => Yii::t('app','Code'),
			'name' => Yii::t('app','Name'),
			'description' => Yii::t('app','Description'),
			'title' => Yii::t('app','Title'),
			'display' => Yii::t('app','Display'),
			'sort_order' => Yii::t('app','Sort Order'),
			'bit_flag' => Yii::t('app','Bit Flag'),
			'modified_flag' => Yii::t('app','Modified Flag'),
			'status_id' => Yii::t('app','Status'),
			'create_time' => Yii::t('app','Create Time'),
			'create_usr_id' => Yii::t('app','Create Usr'),
			'update_time' => Yii::t('app','Update Time'),
			'update_usr_id' => Yii::t('app','Update Usr'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('display',$this->display);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('bit_flag',$this->bit_flag);
		$criteria->compare('modified_flag',$this->modified_flag,true);
		$criteria->compare('status_id',$this->status_id,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_usr_id',$this->create_usr_id,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_usr_id',$this->update_usr_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}