<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use common\models\User;

use common\models\AuthorizationCodes;
use common\models\AccessTokens;

/**
 * Class for common API functions
 */
class Api extends Component
{

    public function sendFailedResponse($message)
    {
        $this->setHeader(400);

        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => $message));

        Yii::$app->end();
    }

	public function sendSuccessResponse($response = false,$additional_info = false)
    {
        $this->setHeader(200);
        
        if (! is_array($response) )
            $response = array();

        if ($additional_info) {
            $response = array_merge($response, $additional_info);
        }
        echo Json::encode($response);
        Yii::$app->end();
    }

    protected function setHeader($status)
    {

        $text = $this->_getStatusCodeMessage($status);

        Yii::$app->response->setStatusCode($status, $text);

        $content_type = "application/json; charset=utf-8";
        header('Content-type: ' . $content_type);
        header('Access-Control-Allow-Origin:*');

    }

    protected function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

}
