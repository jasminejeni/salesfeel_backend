<?php

namespace common\components;
use yii\base\Behavior;
use yii;


class Utility extends Behavior
{
    public function missingUserAgent()
    {


        if (yii::$app->request->userAgent === null)
            return true;


        return false;

    }

    public function generateRequestId()
    {
        return sprintf('%s-%s-%s-%s-%s',
                        $this->_uniqidReal(8),
                        $this->_uniqidReal(4),
                        $this->_uniqidReal(4),
                        $this->_uniqidReal(4),
                        $this->_uniqidReal(12)
        );

    }
    private function _uniqidReal($length = 13) {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }
    
    public static function getUniqueToken($s = null)
	{
	    if ($s === null )
			return  md5(Yii::$app->session->getId() .  microtime() );
		else 
			return  md5($s .  microtime());
	}

} // end class