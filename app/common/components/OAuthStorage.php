<?php 

namespace common\components; 
use yii; 
use yii\base\BaseObject;
use common\models\OAuthAccessToken;
use common\models\OAuthClient;
use common\models\OAuthRefreshToken;
class OAuthStorage extends BaseObject
     implements \OAuth2\Storage\AccessTokenInterface,
                \OAuth2\Storage\ClientCredentialsInterface, 
                \OAuth2\Storage\AuthorizationCodeInterface,
                \OAuth2\Storage\UserCredentialsInterface, 
                \OAuth2\Storage\RefreshTokenInterface
{
    protected $_db;
    protected $_config; 
    protected $_client; 
    protected $_user; 

    public function __construct($config=[])
    {
        $this->_config = array_merge(
            array(
            'client_table' => 'oauth_clients',
            'access_token_table' => 'oauth_access_tokens',
            'refresh_token_table' => 'oauth_refresh_tokens',
            'user_table' => 'user_personal',
            ), $config
        );

    }
    /**
     * ACCESS TOKEN INTERFACE 
     */

    /**
     * Look up the supplied oauth_token from storage.
     *
     * We need to retrieve access token data as we create and verify tokens.
     *
     * @param string $oauth_token - oauth_token to be check with.
     *
     * @return array|null - An associative array as below, and return NULL if the supplied oauth_token is invalid:
     * @code
     *     array(
     *         'expires'   => $expires,   // Stored expiration in unix timestamp.
     *         'client_id' => $client_id, // (optional) Stored client identifier.
     *         'user_id'   => $user_id,   // (optional) Stored user identifier.
     *         'scope'     => $scope,     // (optional) Stored scope values in space-separated string.
     *         'id_token'  => $id_token   // (optional) Stored id_token (if "use_openid_connect" is true).
     *     );
     * @endcode
     *
     * @ingroup oauth2_section_7
     */
    public function getAccessToken($oauth_token) 
    {
        // retrieve from  $this->config['access_token_table'];

        $data  =  OAuthAccessToken::findOne(['access_token'=>$oauth_token]);


        if ($data)
            return $data->toArray();
        else
            return null;

    }

    /**
     * Store the supplied access token values to storage.
     *
     * We need to store access token data as we create and verify tokens.
     *
     * @param string $oauth_token - oauth_token to be stored.
     * @param mixed  $client_id   - client identifier to be stored.
     * @param mixed  $user_id     - user identifier to be stored.
     * @param int    $expires     - expiration to be stored as a Unix timestamp.
     * @param string $scope       - OPTIONAL Scopes to be stored in space-separated string.
     *
     * @return  BOOL true if access token was set, false if not 
     * @ingroup oauth2_section_4
     */



    public function setAccessToken($oauth_token, $client_id, $user_id, $expires, $scope = null)
    {


        $model = OAuthAccessToken::findOne(['access_token'=>$oauth_token]);
        if ($model === null)
        {
            $model = new OAuthAccessToken();
        }

        $data =['OAuthAccessToken'=>[
            'access_token'=>$oauth_token,
            'client_id'=>$client_id,
            'user_id'=>$user_id,
            'expires'=>$expires,
            'scope'=>$scope
        ]];
        $model->load($data);
        if ($model->validate()) {
            return $model->save(false);
        }


        return false;





        
    }

    /**
     * Expire an access token.
     *
     * This is not explicitly required in the spec, but if defined in a draft RFC for token
     * revoking (RFC 7009) https://tools.ietf.org/html/rfc7009
     *
     * @param string $access_token - Access token to be expired
     *                 
     * @return  BOOL true if an access token was unset, false if not
     * @ingroup oauth2_section_6
     *
     * @todo v2.0 include this method in interface. Omitted to maintain BC in v1.x
     */
    public function unsetAccessToken($access_token)
    {

    }

    /** 
     * CLIENT CREDENTIALS INTERFACE 
     */
     
     /**
      * Make sure that the client credentials is valid.
      *
      * @param mixed $client_id - Client identifier to be check with.
      * @param mixed $client_secret - (optional) If a secret is required, 
      *                              check that they've given the right one.
      * 
      *
      * @return TRUE if the client credentials are valid, 
      *                      and MUST return FALSE if it isn't.
      * 
      * @endcode
      *
      * @see http://tools.ietf.org/html/rfc6749#section-3.1
      *
      * @ingroup oauth2_section_3
      */
    public function checkClientCredentials($client_id, $client_secret = null)
    {


        $client = $this->_getClient($client_id);

        return $client && $client['client_secret'] == $client_secret;
        
        
    }
    /**
     * Determine if the client is a "public" client, and therefore
     * does not require passing credentials for certain grant types
     *
     * @param varchar $client_id -Client identifier to be check with.
     * 
     *
     * @return TRUE if the client is public, and FALSE if it isn't
     * 
     * @endcode
     *
     * @see http://tools.ietf.org/html/rfc6749#section-2.3
     * @see https://github.com/bshaffer/oauth2-server-php/issues/257
     *
     * @ingroup oauth2_section_2
     */
    public function isPublicClient($client_id)
    {
            /*
             Currently we are not accepting public clients 
             so return FALSE by default. - Spi March 28 2018 
            */ 
            return false; 
    }
    
    /** 
     * AUTHORIZATION CODE INTERFACE 
     */
     
     /**
     * The Authorization Code grant type supports a response type of "code".
     *
     * @var string
     * @see http://tools.ietf.org/html/rfc6749#section-1.4.1
     * @see http://tools.ietf.org/html/rfc6749#section-4.2
     */
    //const RESPONSE_TYPE_CODE = "code";

    /**
     * Fetch authorization code data (probably the most common grant type).
     *
     * Retrieve the stored data for the given authorization code.
     *
     * Required for OAuth2::GRANT_TYPE_AUTH_CODE.
     *
     * @param $code
     * Authorization code to be check with.
     *
     * @return
     * An associative array as below, and NULL if the code is invalid
     * @code
     * return array(
     *     "client_id"    => CLIENT_ID,      // REQUIRED Stored client identifier
     *     "user_id"      => USER_ID,        // REQUIRED Stored user identifier
     *     "expires"      => EXPIRES,        // REQUIRED Stored expiration in unix timestamp
     *     "redirect_uri" => REDIRECT_URI,   // REQUIRED Stored redirect URI
     *     "scope"        => SCOPE,          // OPTIONAL Stored scope values in space-separated string
     * );
     * @endcode
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.1
     *
     * @ingroup oauth2_section_4
     */
    public function getAuthorizationCode($code)
    {



    }

    /**
     * Take the provided authorization code values and store them somewhere.
     *
     * This function should be the storage counterpart to getAuthCode().
     *
     * If storage fails for some reason, we're not currently checking for
     * any sort of success/failure, so you should bail out of the script
     * and provide a descriptive fail message.
     *
     * Required for OAuth2::GRANT_TYPE_AUTH_CODE.
     *
     * @param string $code         - Authorization code to be stored.
     * @param mixed  $client_id    - Client identifier to be stored.
     * @param mixed  $user_id      - User identifier to be stored.
     * @param string $redirect_uri - Redirect URI(s) to be stored in a space-separated string.
     * @param int    $expires      - Expiration to be stored as a Unix timestamp.
     * @param string $scope        - OPTIONAL Scopes to be stored in space-separated string.
     *
     * @ingroup oauth2_section_4
     */
    public function setAuthorizationCode($code, $client_id, $user_id, $redirect_uri, $expires, $scope = null)
    {



    }

    /**
     * once an Authorization Code is used, it must be expired
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.1.2
     *
     *    The client MUST NOT use the authorization code
     *    more than once.  If an authorization code is used more than
     *    once, the authorization server MUST deny the request and SHOULD
     *    revoke (when possible) all tokens previously issued based on
     *    that authorization code
     *
     */
    public function expireAuthorizationCode($code)
    {
        
    }

    public function getRefreshToken($refresh_token)
    {

        $data  =  OAuthRefreshToken::findOne(['refresh_token'=>$refresh_token]);
        if ($data)
            return $data->toArray();
        else
            return null;

    }

   
    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null)
    {

        $model = new OAuthRefreshToken();
        $data =
            ['OAuthRefreshToken'=>[
                'refresh_token'=>$refresh_token,
                'client_id'=>$client_id,
                'user_id'=>$user_id,
                'expires'=>$expires,
                'scope'=>$scope,
            ]];

        $model->load($data);
        if ($model->validate()) {
            return $model->save(false);
        }

        return false;





    }

    
    public function unsetRefreshToken($refresh_token)
    {

    }

    

      
    public function getClientDetails($client_id)
    {
        return $this->_getClient($client_id);



    }

    /**
     * Get the scope associated with this client
     *
     * @return
     * STRING the space-delineated scope list for the specified client_id
     */
    public function getClientScope($client_id)
    {

    }

    /**
     * Check restricted grant types of corresponding client identifier.
     *
     * If you want to restrict clients to certain grant types, override this
     * function.
     *
     * @param $client_id
     * Client identifier to be check with.
     * @param $grant_type
     * Grant type to be check with
     *
     * @return
     * TRUE if the grant type is supported by this client identifier, and
     * FALSE if it isn't.
     *
     * @ingroup oauth2_section_4
     */
    public function checkRestrictedGrantType($client_id, $grant_type)
    {
        return true; 
    }
    
    /**
     * Get the client object and ensure it is resuable 
     *
     * @param $client_id - Client identifier to retrieve 
     * @return object if client exits or false if there is no client 
     */
    protected function _getClient($client_id) 
    {

        if ($this->_client === null) {
            $this->_client = OAuthClient::findOne(['client_id' => $client_id]);



        }
        if ($this->_client === null)
            return false;
        else
            return $this->_client->toArray();


    }
    /**
     * Get the client object and ensure it is resuable 
     *
     * @param $client_id - Client identifier to retrieve 
     * @return object if client exits or false if there is no client 
     */
    protected function _getUser($username) 
    {

       // $_user = null;
        if ($this->_user === null) {
            $this->_user = \common\models\User::findOne(['username' => $username]);
        }

        if ($this->_user === null)
           return false;

        return $this->_user->toArray();


    }

    /* USER CREDENTIALS INTERFACE */ 
     /**
     * Grant access tokens for basic user credentials.
     *
     * Check the supplied username and password for validity.
     *
     * You can also use the $client_id param to do any checks required based
     * on a client, if you need that.
     *
     * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
     *
     * @param $username
     * Username to be check with.
     * @param $password
     * Password to be check with.
     *
     * @return
     * TRUE if the username and password are valid, and FALSE if it isn't.
     * Moreover, if the username and password are valid, and you want to
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.3
     *
     * @ingroup oauth2_section_4
     */
    public function checkUserCredentials($username, $password) 
    {

        $user = $this->_getUser($username);

        //sha1($password)
        if ($user) {
            $match = (sha1($password) ==  $user['password']);
            // the statements below should be uncommented when we update the database

            if ($user['hash_type'] == 'sha1')
            {
                $match = (sha1($password) ==  $user['password']);

            }
            else
            {
                $match = Yii::$app->getSecurity()->validatePassword($password, $user['password']);

            }

            return $user && $match;
        } 
        return false; 
    }
    public function userCredentialsValidate($username, $password) 
    {

        $user = $this->_getUser($username);

        if ($user && !empty($password)) 
        {
            
            if ($user['hash_type'] == 'sha1')
            {
                $match = (sha1($password) ==  $user['password']);

            }
            else
            {
                $match = Yii::$app->getSecurity()->validatePassword($password, $user['password']);

            }
            if(!$match)
            {
                $data = Yii::t('app', Yii::$app->params['invalidPassword']);
            }
            else
            {
                $data='';
            }

        }
        else
        {
             $data = Yii::t('app', Yii::$app->params['emailUsernameNotRecognised']);
        }
        return $data;
    }
    /**
     * @param string $username - username to get details for
     * @return array|false     - the associated "user_id" and optional "scope" values
     *                           This function MUST return FALSE if the requested user does not exist or is
     *                           invalid. "scope" is a space-separated list of restricted scopes.
     * @code
     *     return array(
     *         "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
     *         "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
     *     );
     * @endcode
     */
    public function getUserDetails($username)
    {
        return $this->_getUser($username);

    }

    public function getUserIdFromRefreshToken($refresh_token)
    {

        $data = $this->getRefreshToken($refresh_token);
        if ($data)
            return $data['user_id'];

        return false;

    }

    public function validateTransientKey($transientKey)
    {
        return true;
    }


} // end class 