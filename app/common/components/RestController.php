<?php


 namespace common\components;

use yii;
use common\components\HttpError;
use common\components\Utility;


abstract class RestController extends \yii\base\Controller
{
    
    /** 
     * This is a comment
     * 
     * @return void 
     */
    public $enableCsrfValidation;
    public $allowedEditTime;
	public $allowedEditTimeMinSec;

    public function init()
    {
        parent::init();
        $this->allowedEditTime = 20 * 60;



        if ($this->missingUserAgent())
            throw new \yii\web\BadRequestHttpException('invalid_user_agent');

        // set the headers
        Yii::$app->response->headers->set('X-Request-Id', $this->generateRequestId());
        Yii::$app->response->headers->set('Cache-Control', 'no-store');
        Yii::$app->response->headers->set('Pragma', 'no-cache');
        //Yii::$app->user->enableSession = false;

    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => yii\filters\auth\HttpBearerAuth::className(),
            'except'=>['token','info','register','reset']
        ];
        $behaviors['httpError'] =[
            'class' => \common\components\HttpError::className(),
        ];

        $behaviors['utility'] = [
            'class' => \common\components\Utility::className(),

        ];
        

        return $behaviors;
    }


    /** 
     * This is a comment that takes postArray
     * 
     * @param array $postArray 
     * @param array $existingArray 
    
     * @return void 
     */
    public function getLogMessage($postArray, $existingArray)
    {
        $arr = array_diff($postArray, $existingArray);
        $s = '';
        foreach ($arr as $k => $v) {
                $s .= sprintf('%s %s <br />', $k, $v);
        }
        return $s;
    }
    

    protected function paginate(&$dataProvider)
    {
        return
            [
                'per_page'=>$dataProvider->pagination->pageSize,
                'page'=>($dataProvider->pagination->page + 1),
                'page_count'=>$dataProvider->pagination->pageCount
            ] ;

    }
    

    /** 
     * This is a comment that takes postArray
     * 
     * @param mixed $data 
     * 
     * @return void 
     */
    protected function sendContent($data)
    {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            \Yii::$app->response->data  =  $data;
    }
}
