<?php

namespace common\components;
use yii;


class Types
{
    public static function getTypeValue($type, $key, $id){
		$result = null ;
        $arr = array(); 
        
    	switch (strtolower($type)){
			case 'modeltype':
                $arr = self::$modelType;
                break;
			case 'status':
				$arr = self::$status;
                break;
            default:
				break;
        }

        foreach( $arr as $row)
		{
			if (intval($row['id']) === intval($id))
				$result = $row[$key]; 
		}
		return $result; 
    }
    
    public static $status = [
        'active'=>['id'=>1, 'code'=>'active' , 'name'=> 'Active', 'description'=>'Active' , 'title'=>'' ],
        'inactive'=>['id'=>3, 'code'=>'inactive' , 'name'=> 'Inactive', 'description'=>'Inactive' , 'title'=>'' ],
        'deleted'=>['id'=>4, 'code'=>'deleted' , 'name'=>'Deleted' , 'description'=>'Deleted' , 'title'=>'' ],
        'disabled'=>['id'=>5, 'code'=>'disabled', 'name'=>'Disabled' , 'description'=>'Disabled' , 'title'=>'' ],
        'archived'=>['id'=>6, 'code'=>'archived', 'name'=> 'Archived', 'description'=>'Archived' , 'title'=>'' ],
        'cancelled'=>['id'=>7, 'code'=>'cancelled', 'name'=>'Cancelled' , 'description'=>'Cancelled' , 'title'=>'' ],
        'pending'=>['id'=>8, 'code'=>'pending', 'name'=>'Pending' , 'description'=>'Pending' , 'title'=>'' ],
        'draft'=>['id'=>9, 'code'=>'draft', 'name'=>'Draft' , 'description'=>'Draft' , 'title'=>'' ],
        'restricted'=>['id'=>10, 'code'=>'restricted', 'name'=>'Restricted', 'description'=>'Restricted', 'title'=>'' ],
    ];

    
    public static $modelType = [
        'null'=>['id'=>1, 'code'=>'null' , 'name'=>'Select from...' , 'description'=>'NULL' , 'title'=>'NULL' ],
    ];
    
    public static $boolean = [
        'null'=>['id'=>1, 'code'=>null , 'name'=>'Select from...' , 'description'=>'No value' , 'title'=>'' ],
        'true'=>['id'=>2, 'code'=>1 , 'name'=> 'True', 'description'=>'Yes' , 'title'=>'' ],
        'false'=>['id'=>3, 'code'=>0 , 'name'=> 'False', 'description'=>'No' , 'title'=>'' ],
    ];
    
        
    public static $actionType = [
		'null'=>['id'=>1, 'code'=>'null' , 'name'=>'Select from...' , 'description'=>'No value' , 'title'=>'' ],
		'insert'=>['id'=>2, 'code'=>'insert' , 'name'=> 'insert', 'description'=>'insert' , 'title'=>'' ],
		'update'=>['id'=>3, 'code'=>'update' , 'name'=> 'update', 'description'=>'update' , 'title'=>'' ],
		'delete'=>['id'=>4, 'code'=>'delete' , 'name'=> 'delete', 'description'=>'delete' , 'title'=>'' ],
		'join'=>['id'=>5, 'code'=>'join' , 'name'=> 'join', 'description'=>'join' , 'title'=>'' ],
		'leave'=>['id'=>6, 'code'=>'leave' , 'name'=> 'leave', 'description'=>'leave' , 'title'=>'' ],
		'login'=>['id'=>7, 'code'=>'login' , 'name'=> 'login', 'description'=>'login' , 'title'=>'' ],
		'login_failed'=>['id'=>8, 'code'=>'login_failed' , 'name'=> 'login_failed', 'description'=>'login_failed' , 'title'=>'' ],
		'login_invalid'=>['id'=>9, 'code'=>'login_invalid' , 'name'=> 'login_invalid', 'description'=>'login_invalid' , 'title'=>'' ],
		'logout'=>['id'=>10, 'code'=>'logout' , 'name'=> 'logout', 'description'=>'logout' , 'title'=>'' ],
		'register'=>['id'=>11, 'code'=>'register' , 'name'=> 'Register', 'description'=>'Registration of user' , 'title'=>'User registration' ],
		'error'=>['id'=>12, 'code'=>'error' , 'name'=> 'Error', 'description'=>'Error raised in system' , 'title'=>'Error raised' ],
		'review'=>['id'=>13, 'code'=>'review' , 'name'=> 'Review', 'description'=>'Content Reviewed' , 'title'=>'Content Reviewed' ],
		'publish'=>['id'=>14, 'code'=>'publish' , 'name'=> 'Publish', 'description'=>'Content Published' , 'title'=>'Content Published' ],
		'pwd_changed'=>['id'=>15, 'code'=>'pwd_changed' , 'name'=> 'Password Changed', 'description'=>'Password Changed' , 'title'=>'Password Changed' ],
        ];
} // end class