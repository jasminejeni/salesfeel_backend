<?php

namespace common\components;

use yii\base\Behavior;
use yii;

class HttpError extends Behavior
{


    protected function getCodes()
    {

        return [
            'invalid_request' => ['statusCode' => 401, 'statusText' => 'Unauthorised', 'description' => Yii::t('app',Yii::$app->params['invalidToken'])],
            'invalid_client' => ['statusCode' => 401, 'statusText' => 'Unauthorised', 'description' => Yii::t('app',Yii::$app->params['invalidUsernamePassword'])],
            'invalid_grant' => ['statusCode' => 401, 'statusText' => 'Unauthorised', 'description' => Yii::t('app',Yii::$app->params['invalidUsernamePassword'])],
            'unauthorised_client' => ['statusCode' => 400, 'description' => ''],
            'invalid_scope' => ['statusCode' => 400, 'description' => ''],
            'invalid_code' => ['statusCode' => 404, 'statusText' => 'Not found', 'description' => Yii::t('app',Yii::$app->params['invalidAccessCode'])],

        ];
    }


    public function errorEnvelope()
    {

    }

    public  function throwHttpException($code)
    {


        Yii::$app->response->stream = ['name'=>'value'];
        if (isset($this->codes[$code])) {
            throw new yii\web\HttpException($this->codes[$code]['statusCode'],
                sprintf('%s', $this->codes[$code]['description'])
            );
        }
        else
            return false;


    }


} // end class