<?php

namespace common\components;
use yii;
/**
 * oauth2 module definition class
 */
class XModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\oauth2\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {

         parent::init();

        // custom initialization code goes here
    }
}
