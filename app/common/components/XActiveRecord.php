<?php


namespace common\components;
use yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

class XActiveRecord extends \yii\db\ActiveRecord
{
    public function behaviors()
    {

        return [
           /* 'timestamp'=> [
                'class'=>TimestampBehavior::className(),
                'attributes'=>[
                    parent::EVENT_BEFORE_INSERT => ['created_at'],
                    parent::EVENT_BEFORE_UPDATE => ['updated_at'],

                ]
            ],*/

            'blameable'=>[
                'class'=>BlameableBehavior::className(),
                'createdByAttribute'=>'created_by',
                'updatedByAttribute'=>'updated_by',


            ],
        ];




    }
}